package io.jpress.addon.rotatingpicture.directive;

import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jpress.addon.rotatingpicture.model.JpressAddonRotatingpicture;
import io.jpress.addon.rotatingpicture.service.JpressAddonRotatingpictureService;

import java.util.List;


@JFinalDirective("jpressAddonRotatingpictureList")
public class RotatingPictureDirective extends JbootDirectiveBase {

    @Inject
    private JpressAddonRotatingpictureService service;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        List<JpressAddonRotatingpicture> all = service.findAll();
        scope.setLocal("jpressAddonRotatingpictureList", all);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
